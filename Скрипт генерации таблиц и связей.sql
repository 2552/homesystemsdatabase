USE [HomeSystems]
GO
/****** Object:  Table [dbo].[Договор]    Script Date: 05.06.2021 21:45:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Договор](
	[Id] [int] NOT NULL,
	[ЗаказId] [int] NOT NULL,
	[НомерДоговора] [nvarchar](50) NOT NULL,
	[ТекстДоговора] [nvarchar](max) NULL,
 CONSTRAINT [PK_Договор] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Заказчик]    Script Date: 05.06.2021 21:45:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Заказчик](
	[Id] [int] NOT NULL,
	[ИмяЗаказчика] [nvarchar](50) NOT NULL,
	[ФамилияЗаказчика] [nvarchar](50) NOT NULL,
	[НомерПаспорта] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Заказчик] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Заказы]    Script Date: 05.06.2021 21:45:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Заказы](
	[Id] [int] NOT NULL,
	[ЗаказчикId] [int] NOT NULL,
	[НомерЗаказа] [int] NOT NULL,
	[КодЗаказа] [int] NOT NULL,
 CONSTRAINT [PK_Заказы] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ИсполнениеУслуги]    Script Date: 05.06.2021 21:45:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ИсполнениеУслуги](
	[УслугиId] [int] NOT NULL,
	[ЗаказыId] [int] NOT NULL,
	[ПодрядчикId] [int] NULL,
	[СотрудникиId] [int] NULL,
	[Id] [int] NOT NULL,
	[Цена] [money] NOT NULL,
 CONSTRAINT [PK_ИсполнениеУслуги] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ИсторияСтатусов]    Script Date: 05.06.2021 21:45:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ИсторияСтатусов](
	[ИсполнениеУслугиId] [int] NOT NULL,
	[СтатусId] [int] NOT NULL,
	[Дата] [date] NOT NULL,
 CONSTRAINT [PK_ИсторияСтатусов] PRIMARY KEY CLUSTERED 
(
	[ИсполнениеУслугиId] ASC,
	[СтатусId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Объект]    Script Date: 05.06.2021 21:45:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Объект](
	[Id] [int] NOT NULL,
	[Описание] [nvarchar](50) NOT NULL,
	[Адрес] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Объект] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ОбъектыЗаказа]    Script Date: 05.06.2021 21:45:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ОбъектыЗаказа](
	[ЗаказId] [int] NOT NULL,
	[ОбьектId] [int] NOT NULL,
 CONSTRAINT [PK_ОбъектыЗаказа] PRIMARY KEY CLUSTERED 
(
	[ЗаказId] ASC,
	[ОбьектId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Подрядчик]    Script Date: 05.06.2021 21:45:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Подрядчик](
	[Id] [int] NOT NULL,
	[НазваниеОрганизации] [nvarchar](50) NOT NULL,
	[Специализация] [nvarchar](50) NOT NULL,
	[ИНН] [nvarchar](10) NOT NULL,
	[КПП] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Подрядчик] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Поставщик]    Script Date: 05.06.2021 21:45:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Поставщик](
	[Id] [int] NOT NULL,
	[НазваниеОрганизации] [nvarchar](50) NOT NULL,
	[Специализация] [nvarchar](50) NOT NULL,
	[ИНН] [int] NOT NULL,
	[КПП] [int] NOT NULL,
 CONSTRAINT [PK_Поставщик] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ПоставщикТовар]    Script Date: 05.06.2021 21:45:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ПоставщикТовар](
	[ПоставщикId] [int] NULL,
	[ТоварId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Сотрудники]    Script Date: 05.06.2021 21:45:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Сотрудники](
	[Id] [int] NOT NULL,
	[Имя] [nvarchar](50) NOT NULL,
	[Фамилия] [nvarchar](50) NOT NULL,
	[Отчество] [nvarchar](50) NULL,
	[Должность] [nvarchar](50) NOT NULL,
	[Зарплата] [money] NOT NULL,
 CONSTRAINT [PK_Сотрудники] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Статус]    Script Date: 05.06.2021 21:45:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Статус](
	[Id] [int] NOT NULL,
	[НазваниеСтатуса] [nvarchar](50) NOT NULL,
	[КодСтатуса] [int] NOT NULL,
 CONSTRAINT [PK_Статус] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Товар]    Script Date: 05.06.2021 21:45:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Товар](
	[Id] [int] NOT NULL,
	[ИсполнениеУслугиId] [int] NOT NULL,
	[ПоставщикId] [int] NOT NULL,
	[СерийныйНомер] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Товар] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Услуги]    Script Date: 05.06.2021 21:45:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Услуги](
	[Id] [int] NOT NULL,
	[НазваниеУслуги] [nvarchar](50) NOT NULL,
	[КодУслуги] [int] NOT NULL,
	[Сложность] [int] NOT NULL,
 CONSTRAINT [PK_Услуги] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Услуги] ADD  CONSTRAINT [DF_Услуги_Сложность]  DEFAULT ((1)) FOR [Сложность]
GO
ALTER TABLE [dbo].[Договор]  WITH CHECK ADD  CONSTRAINT [FK_Договор_Заказы] FOREIGN KEY([ЗаказId])
REFERENCES [dbo].[Заказы] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Договор] CHECK CONSTRAINT [FK_Договор_Заказы]
GO
ALTER TABLE [dbo].[Заказы]  WITH CHECK ADD  CONSTRAINT [FK_Заказы_Заказчик] FOREIGN KEY([ЗаказчикId])
REFERENCES [dbo].[Заказчик] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Заказы] CHECK CONSTRAINT [FK_Заказы_Заказчик]
GO
ALTER TABLE [dbo].[ИсполнениеУслуги]  WITH CHECK ADD  CONSTRAINT [FK_ИсполнениеУслуги_Заказы] FOREIGN KEY([ЗаказыId])
REFERENCES [dbo].[Заказы] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ИсполнениеУслуги] CHECK CONSTRAINT [FK_ИсполнениеУслуги_Заказы]
GO
ALTER TABLE [dbo].[ИсполнениеУслуги]  WITH CHECK ADD  CONSTRAINT [FK_ИсполнениеУслуги_Подрядчик] FOREIGN KEY([ПодрядчикId])
REFERENCES [dbo].[Подрядчик] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ИсполнениеУслуги] CHECK CONSTRAINT [FK_ИсполнениеУслуги_Подрядчик]
GO
ALTER TABLE [dbo].[ИсполнениеУслуги]  WITH CHECK ADD  CONSTRAINT [FK_ИсполнениеУслуги_Сотрудники] FOREIGN KEY([СотрудникиId])
REFERENCES [dbo].[Сотрудники] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ИсполнениеУслуги] CHECK CONSTRAINT [FK_ИсполнениеУслуги_Сотрудники]
GO
ALTER TABLE [dbo].[ИсполнениеУслуги]  WITH CHECK ADD  CONSTRAINT [FK_ИсполнениеУслуги_Услуги] FOREIGN KEY([УслугиId])
REFERENCES [dbo].[Услуги] ([Id])
GO
ALTER TABLE [dbo].[ИсполнениеУслуги] CHECK CONSTRAINT [FK_ИсполнениеУслуги_Услуги]
GO
ALTER TABLE [dbo].[ИсторияСтатусов]  WITH CHECK ADD  CONSTRAINT [FK_ИсторияСтатусов_ИсполнениеУслуги] FOREIGN KEY([ИсполнениеУслугиId])
REFERENCES [dbo].[ИсполнениеУслуги] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ИсторияСтатусов] CHECK CONSTRAINT [FK_ИсторияСтатусов_ИсполнениеУслуги]
GO
ALTER TABLE [dbo].[ИсторияСтатусов]  WITH CHECK ADD  CONSTRAINT [FK_ИсторияСтатусов_Статус] FOREIGN KEY([СтатусId])
REFERENCES [dbo].[Статус] ([Id])
GO
ALTER TABLE [dbo].[ИсторияСтатусов] CHECK CONSTRAINT [FK_ИсторияСтатусов_Статус]
GO
ALTER TABLE [dbo].[ОбъектыЗаказа]  WITH CHECK ADD  CONSTRAINT [FK_ОбъектыЗаказа_Заказы] FOREIGN KEY([ЗаказId])
REFERENCES [dbo].[Заказы] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ОбъектыЗаказа] CHECK CONSTRAINT [FK_ОбъектыЗаказа_Заказы]
GO
ALTER TABLE [dbo].[ОбъектыЗаказа]  WITH CHECK ADD  CONSTRAINT [FK_ОбъектыЗаказа_Объект] FOREIGN KEY([ОбьектId])
REFERENCES [dbo].[Объект] ([Id])
GO
ALTER TABLE [dbo].[ОбъектыЗаказа] CHECK CONSTRAINT [FK_ОбъектыЗаказа_Объект]
GO
ALTER TABLE [dbo].[ПоставщикТовар]  WITH CHECK ADD  CONSTRAINT [FK_ПоставщикТовар_Поставщик] FOREIGN KEY([ТоварId])
REFERENCES [dbo].[Товар] ([Id])
GO
ALTER TABLE [dbo].[ПоставщикТовар] CHECK CONSTRAINT [FK_ПоставщикТовар_Поставщик]
GO
ALTER TABLE [dbo].[ПоставщикТовар]  WITH CHECK ADD  CONSTRAINT [FK_ПоставщикТовар_Товар] FOREIGN KEY([ТоварId])
REFERENCES [dbo].[Товар] ([Id])
GO
ALTER TABLE [dbo].[ПоставщикТовар] CHECK CONSTRAINT [FK_ПоставщикТовар_Товар]
GO
ALTER TABLE [dbo].[Товар]  WITH CHECK ADD  CONSTRAINT [FK_Товар_ИсполнениеУслуги] FOREIGN KEY([ИсполнениеУслугиId])
REFERENCES [dbo].[ИсполнениеУслуги] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Товар] CHECK CONSTRAINT [FK_Товар_ИсполнениеУслуги]
GO
ALTER TABLE [dbo].[Товар]  WITH CHECK ADD  CONSTRAINT [FK_Товар_Поставщик] FOREIGN KEY([ПоставщикId])
REFERENCES [dbo].[Поставщик] ([Id])
GO
ALTER TABLE [dbo].[Товар] CHECK CONSTRAINT [FK_Товар_Поставщик]
GO
